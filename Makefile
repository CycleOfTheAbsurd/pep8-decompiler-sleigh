all: sleigh_spec/data/languages/pep8.sla

deps:
	pip2 install -r Unit-Testing/requirements.txt

sleigh_spec/data/languages/pep8.sla: sleigh_spec/data/languages/pep8.slaspec sleigh_spec/data/languages/pep8.sinc sleigh_spec/data/languages/pep8.cspec sleigh_spec/data/languages/pep8.pspec sleigh_spec/data/languages/pep8.ldefs
	$(GHIDRA_INSTALL_DIR)support/sleigh $< $@
	chmod +r $@

install: all
	mkdir -p $(GHIDRA_INSTALL_DIR)Ghidra/Processors/pep8/
	cp -r sleigh_spec/* $(GHIDRA_INSTALL_DIR)Ghidra/Processors/pep8/

check:
	$(MAKE) -C tests/ $@

uninstall:
	rm -rf $(GHIDRA_INSTALL_DIR)Ghidra/Processors/pep8/

clean:
	rm -f sleigh_spec/data/languages/pep8.sla
	$(MAKE) -C tests/ $@
	$(MAKE) -C documents/ $@

documents:
	$(MAKE) -C documents/ all
