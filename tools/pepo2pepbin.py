#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from binascii import unhexlify
from sys import argv

if len(argv) not in [2,3]:
    print("Usage: pepo2pepbin <filename.pepo> [<outfile.pepbin>]")
infile = argv[1]
outfile = infile.replace("pepo", "pepbin")
if len(argv) == 3:
    outfile = argv[2]

with open(infile, "r") as f:
    content = f.read().replace(" ", "").replace("\n", "")[:-2]
binary = unhexlify(content)

with open(outfile, "wb") as f:
    f.write(binary)
