\title{Spécification SLEIGH pour décompiler des programmes PEP/8}
\author{Alexandre Côté Cyr}
\institute{UQAM - INF889A}
\maketitle

# Contexte: PEP/8

 - Language d'assemblage et simulateur
 - Créé par J. Stanley Warford de _Pepperdine University_
 - Article publié en 2007 par Warford et Ryan Okelberry de _Novell_[^1]
 - Est écrit directement, pas de compilateur vers du PEP/8
 - Enseigné à l'UQAM

[^1]: J. Stanley Warford and Ryan Okelberry. 2007. Pep8CPU: a programmable simulator for a central processing unit. SIGCSE Bull. 39, 1 (March 2007), 288–292. DOI:https://doi.org/10.1145/1227504.1227413

# PEP/8: Caractéristiques techniques

 - Architecture Von Neumann
 - 16bits avec Gros-boutisme (_big endian_)
 - Drapeaux (_Flags_) pour Zero(Z), Négatif(N), Retenue(C) et Débordement(V)
 - Seulement 2 registres à usage général
	 - \+ pointeur d'instruction et de pile
 - 39 instructions de base avec variantes pour les registres et mode d'adressage
 	- 8 modes d'adressage

(Par comparaison: 8086 a 8 registres, 9 _flags_ et >100 instructions)

# Contexte: SLEIGH

 - Langage pour définir des processeurs
 - Utilisé par Ghidra
 - Compilé vers P-Code, un _Register Transfer Language_

# Register transfer language

Représentation intermédiaire qui montre le flux des données au niveau des registres

![Exemple de RTL fourni dans la documentation](./PEP8_RTL.png)

# Objectif du projet

 - __Objectif initial__: Écrire une définition SLEIGH pour PEP/8 ~~et un petit décompilateur CLI qui utilise SLEIGH~~
 	- Mais: Documentation pour SLEIGH comme bibliothèque a disparu
 - __Objectif revu__: Définition SLEIGH complète ~~et utiliser Ghidra en CLI pour obtenir le résultat~~
 	- Mais: Définir les instructions sans pouvoir tester à mesure c'est une excellente façon de se planter
 - __Objectif revu v2__: Définition SLEIGH complète et _framework_ de tests unitaires pour le comportement d'instruction. Utiliser Ghidra pour obtenir le résultat de la décompilation

# Désassemblage

Relativement simple. Il suffit de fournir:

 - Définition des registres et de leur tokenisation

![Tokenisation](./PEP8_Token.png)

 - Définition des _opcodes_ et de comment représenter chaque instruction

```SLEIGH
:BR operand,addr_mode1 is opcode7=0x02 & addr_mode1 & operand & dest
:NOT^regN^ is opcodeN7=0x0C & regN
:DECI operand,addr_mode3 is opcode5=0x06 & operand & addr_mode3
```

# Démo désassemblage

![](./live_demo.png)

# Décompilation

SLEIGH et le moteur de décompilation s'en chargent. Algorithmes déjà définis

On doit définir le comportement et les effets de chacune des instructions

# Framework de tests unitaires

Ghidra utilise Jython

Difficile d'interfacer avec les bibliothèques de Ghidra ---> Script python qui roule dans Ghidra

 - On utilise l'émulation du PCode pour valider le résultat vs le résultat attendu pour chaque instruction
 - Fichier JSON5 pour le résultat attendu (extension de JSON qui supporte les nombres hexadécimaux)

En ce moment, on peut valider:

 - Valeur des registres (incluant `pc` et `sp`)
 - État des _flags_
 - Valeurs littérales sur la pile
 - Valeurs à des adresses mémoires arbitraires

Créé un fichier `.gitlab-ci.yml` qui installe les dépendances et roule le build et les tests pour faire de l'intégration continue

---

::: columns

:::: column
### Programme PEP/8 test
```
LDA 0x1234,i
STA testst,d
STBYTEA teststb,d
STOP

testst: .BLOCK 2
teststb: .BLOCK 1
.END
```
::::

:::: column
### Exemple d'état attendu
```json
[...]
{
	"registers": {
		"rA": 0x1234,
		"rX": null,
		"sp": null,
		"pc": null,
		"NF": 0,
		"ZF": 0,
		"VF": 0,
		"CF": 0
	},
	"stack": null,
	"addr": {
		"0x0A": [0x1234, 2],
		"0x0C": [0x34, 1]
	}
}
[...]
```
::::

:::

---

# Démo décompilation

![](./demo_gods.jpg)

# Problèmes rencontrés

 - Essayé d'intégrer les `jar` de Ghidra/SLEIGH dans: programme en Python3, Python2, avec PyJNIus. Finalement Clojure: ça marche mais la doc n'est plus là.
 - Tenté de créer un registre FLAGS pour tous les regrouper (la fonctionnalité existe dans SLEIGH, mais personne ne l'utilise...)
 - Ghidra ne nous retourne pas les codes de retour des scripts
 - Fichier `pspec` est pas documenté
 - Fichier `cspec` est mal documenté
 	- Code source est immense et complexe
 - Version CLI de PEP/8 avait des restrictions étranges sur les longueurs de chemin vers les fichiers

## Choses qui ont été complexes

 - Gérer les modes d'adressage
 	- d'abord avec des macros, mais ça polluait trop le code décompilé
 - Rouler automatiquement les tests unitaires
 - Gérer la longueur variable des instructions
 - Définir des conventions d'appels qui ont du sens alors qu'il n'en existe pas
 - Comment gérer les _flags_ d'_Overflow_ et de _Carry_
 - Comment gérer les instructions qui sont en fait des _syscalls_? (DECI, DECO, STOP, etc.)

# Ouvertures

 - Implémenter la gestion du _Carry_ et de l'_Overflow_
 - Implémenter les _syscalls_
 - Inclure le `pep8os` dans les fichiers source de Ghidra

 - Généraliser le framework de tests pour le rendre applicable à tous les processeurs
 - Améliorer le logging et l'output pour les erreurs

# Voilà!

Tout mon projet est disponible sur Gitlab sous licence Apache 2.0

https://gitlab.com/CycleOfTheAbsurd/pep8-decompiler-sleigh
