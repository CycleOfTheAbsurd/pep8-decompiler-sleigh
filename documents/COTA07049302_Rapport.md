---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=2cm
fontfamily:
- charter
---
\begin{titlepage}
\begin{center}
    \vspace*{2cm}

    \textbf{Analyse de programmes pour la sécurité logicielle}

        INF889A

    \vspace{3cm}

    \textbf{Alexandre Côté Cyr - COTA07049302}

    \vspace{3cm}

        Projet final\\
        \textbf{Spécification SLEIGH pour décompiler des programmes PEP/8}

    \vspace{3cm}

        Travail remis\\
        à\\
        Jean Privat

    \vfill

    Université du Québec à Montréal\\
    21 Mars 2020

    \vspace*{2cm}
\end{center}
\end{titlepage}

\thispagestyle{empty}
\clearpage
\pagenumbering{arabic}
\setcounter{page}{1}


# Introduction

Ce projet est un décompilateur pour l'architecture de processeur PEP/8. Pour arriver à la décompiler, nous réaliserons une définition SLEIGH de cette architecture. Ceci nous permet d'utiliser les algorithmes de décompilation de Ghidra, plutôt que d'avoir à réimplémenter un algorithme de décompilation.

D'abord, nous expliquerons ce que sont PEP/8 et SLEIGH. Ensuite, nous décrirons la réalisation du projet lui-même en plusieurs sous-étapes: l'implémentation du désassemblage, le développement d'un cadre de tests unitaires pour définitions SLEIGH et l'implémentation de la décompilation. Finalement, nous concluerons  avec des applications pratiques possibles et des possibilités de travaux futurs en lien avec ce projet.


## Objectif du projet

Notre objectif initial était de créer une définition SLEIGH pour PEP/8 ainsi qu'un programme en ligne de commande qui permettrait d'obtenir le résultat du désassemblage et de la décompilation d'un exécutable PEP/8. Ce programme aurait utilisé la bibliothèque SLEIGH et le moteur de décompilation de Ghidra pour arriver à ces résultats.

Très tôt dans la réalisation de notre projet, la documentation expliquant comment intégrer la bibliothèque SLEIGH à un programme a été retirée du site où elle était hébergée. Sans cette documentation, il aurait été laborieux de réaliser le programme voulu. Nous avons donc décidé d'utiliser Ghidra pour le désassemblage et la décompilation.

Durant la réalisation de la définition du comportement des instructions, nous avons remarqué le besoin criant de pouvoir valider ces définitions au fur et à mesure qu'elles sont créées. Il n'existait pas d'outil permettant de faire ces validations automatiquement et une validation manuelle aurait été complexe, longue et prône à l'erreur. Pour ces raisons, nous avons décidé de développer un cadre de tests unitaires qui utilise l'émulateur de P-Code fourni dans Ghidra.


# Concepts préalables

## PEP/8

PEP/8 est une architecture et un langage d'assemblage qui ont été créés spécifiquement pour enseigner l'assembleur.[^1] Il n'existe pas de processeurs qui l'utilisent. L'auteur fournit plutôt un environnement de développement intégré à un simulateur qui permet, entre autres, d'exécuter le code, de le déboguer et de voir l'état de la mémoire.

[^1]: J. Stanley Warford and Ryan Okelberry. 2007. _Pep8CPU: a programmable simulator for a central processing unit_. SIGCSE Bull. 39, 1 (March 2007), 288–292. DOI:https://doi.org/10.1145/1227504.1227413

Nous avons choisi PEP/8 pour notre projet pour deux raisons principales:

1. Il est enseigné à l'UQAM, donc nous étions déjà familier avec celui-ci
2. L'architecture est simple et l'ensemble d'instructions est limité, ce qui limite l'ampleur de la tâche


### Caractéristiques techniques

Le pseudo-processeur PEP/8 suit l'architecture de Von Neumann[^2]. Il utilise une architecture 16 bits avec gros-boutisme et possède seulement 4 registres. Parmis ceux-ci, il y a deux registres à usage général, soit l'accumulateur `A` et le registre d'index `X`. En plus de ceux-ci, nous retrouvons aussi un registre pour le pointeur d'instruction `PC` et un pour le pointeur de pile `SP`. Il possède également 4 _flags_ d'état: Négatif `N`, Zéro `Z`, Débordement `V` et Retenue `C`.

PEP/8 comporte 8 modes d'adressage. Ceux-ci déterminent comment accéder à l'emplacement de la valeur désirée à partir de l'opérande d'une instruction.

L'ensemble d'instruction de PEP/8 comporte 39 instructions de bases. Pour plusieurs d'entres elles, il existe des variantes selon le registre ou le mode d'adressage utilisé.

[^2]: https://en.wikipedia.org/wiki/Von_Neumann_architecture


## SLEIGH

SLEIGH est un langage de définition de processeur ainsi que la librairie et l'ensemble des outils qui permettent d'utiliser ce langage.[^3] Il permet de définir l'architecture d'un processeur et le comportement de ces instructions indépendamment des détails de l'architecture elle-même. Cette définition est ensuite compîlée vers une représentation intermédiaire nommée P-Code.

Le P-Code est un langage de transfert de registres. Son intérêt dans le cadre de notre projet est qu'il est utilisé par le désassembleur et le décompilateur de Ghidra[^4]. L'utilisation de cette représentation intermédiaire permet à ceux-ci d'appliquer leurs algorithmes à un exécutable sans avoir à les redéfinir pour chaque architecture existante.

Pour plus d'information sur SLEIGH, vous pouvez vous référez à [notre travail précédent sur celui-ci](https://gitlab.com/CycleOfTheAbsurd/inf889a-outil-ghidra-sleigh).

[^3]: NSA. _SLEIGH - A Language for Rapid Processor Specification_. (1er septembre 2017). \url{https://ghidra.re/courses/languages/html/sleigh.html}
[^4]: NSA. _Ghidra Software Reverse Engineering Framework_. Github. \url{https://github.com/NationalSecurityAgency/ghidra}


# Réalisation du projet

Nous pouvons découper le projet en trois étapes, chacune ayant son propre sous-objectif.

## Désassemblage

Pour cette partie, notre but est de pouvoir prendre le programme compilé et le représenter correctement avec les mnémoniques du langage d'assemblage.

Il nous faut d'abord définir les registres et espaces mémoires disponibles dans cette architecture (voir lignes 3-18 et 43-45 de [pep8.slaspec](../sleigh_spec/data/languages/pep8.slaspec#L3)). Ensuite, nous devons définir le découpage des instructions en champs; ceci permet d'associer les bits de chaque instruction à ce qu'ils représentent (lignes 20-41 de [pep8.slaspec](../sleigh_spec/data/languages/pep8.slaspec#L20)).

Enfin, nous devons définir comment identifier et représenter chaque instruction. Prenons l'instruction `ADD` en exemple pour démontrer la syntaxe.

| Représentation              | Identification  | Variables                           |
|-----------------------------|-----------------|-------------------------------------|
|`:ADD^reg^ operand,addr_mode3` | `is opcode4=0x07` | `& reg & operand & addr_mode3 & value` |
| Littéral "ADD", suivi du nom du registre (valeur de `reg`) puis de la valeur de l'opérande et du mode d'addressage séparés par une virgule | Le champs `opcode` doit avoir la valeur 7 pour que le désassembleur traite l'intruction comme un `ADD` | L'opération `ADD` utilisera ces champs et ils doivent donc être présents |

Donc, la chaîne de bits `011100000000011000010011` (0x700613) sera représentée comme `ADDrA 1555,i`

Pour ceci, nous nous sommes référés aux deux premières pages de [la documentation](http://computersystemsbook.com/wp-content/uploads/2016/03/ReferenceHandout.pdf) qui définissent le découpage des instructions.[^5]

[^5]: J. Stanley Warford. _Fourth Ed Pep/8 Documentation_. (Février 2010). \url{http://computersystemsbook.com/wp-content/uploads/2016/03/ReferenceHandout.pdf}

Cette étape a été la plus simple à réaliser. La partie la plus complexe a été de trouver comment bien représenter les modes d'addressages.


## Cadriciel de tests unitaires pour SLEIGH

Cette étape nous est apparue comme étant un prérequis à la décompilation. En effet, il n'existe pas de manière de tester simplement si le comportement que nous définissons pour une instruction correspond à son comportement réel. Sans façon de tester celà, le risque de faire des erreurs est très élevé et il peut être ardu de les repérer par la suite.

Notre système de test unitaire utilise la capacité de scriptage de Ghidra.[^6] Ceci permet au script d'accéder, entre autres, à son engin de décompilation et à son émulateur de P-Code. Ce dernier permet d'exécuter directement la représentation intermédiaire de notre programme en P-Code et d'accéder à tous les éléments que nous avons définis pour le processeur en question.[^7] Nous pouvons ensuite comparer les résultats de cette exécution avec ceux de l'exécution réelle du même programme pour valider le comportement de nos instructions.

[^6]: NSA. _Ghidra Scripting_. (28 Février 2019) \url{https://ghidra.re/courses/GhidraClass/Intermediate/Scripting.html}
[^7]: John Toterhi. _Emulating Ghidra’s PCode: Why/How_. \url{https://medium.com/@cetfor/emulating-ghidras-pcode-why-how-dd736d22dfb}

L'approche que nous avons choisie est de fournir une représentation en JSON5 de l'état attendu de notre programme après chacune des instructions (ceci inclue l'état des registres, _flags_, espaces mémoire et de la pile ainsi que la position du pointeur d'instruction). Notre script Python émule le programme en s'arrêtant après chaque instruction pour comparer son état avec celui attendu et lève une erreur s'ils ne concordent pas. Ceci rend possible la création de tests unitaires à l'aide de petits programmes en PEP/8 qui font appel uniquement aux instructions que l'on désire tester et à celles nécessaires pour préparer le dit test.

La création de ce cadriciel de tests nous a posé plusieurs problèmes et celui-ci présente certaines faiblesses.

Il n'existe aucune documentation qui explique comment accéder aux fonctionnalités de Ghidra sans exécuter un script dans son environnement. Nous sommes donc contraint d'utiliser son environnement Jython qui ne supporte qu'une version récemment dépréciée du langage et qui n'inclue que les paquets de base.

Présentement, notre cadre est spécifique à l'architecture PEP/8 et est composé d'un seul monolithe de code fortement couplé. Ceci rend plus complexe son extension.

De plus, la sortie du moteur de scriptage de Ghidra est très bruyante et ne permet pas de retourner le code de sortie du script exécuté. Il est donc plus difficile de voir et d'analyser le résultats des tests. Ceci pourrait être amélioré avec une meilleur configuration de Log4J, le bibliothèque utilisée pour la journalisation.

La définition des états attendus en JSON5 est relativement verbeuse et doit être rédigée à la main. Ceci rend la création de tests plus longue et augmente le risque d'erreur humaine. Un bon moyen de réduire le travail nécessaire serait de générer automatiquement ces états à partir d'une exécution réelle du programme ou d'une trace d'exécution.


## Décompilation

Pour cette dernière étape, l'objectif est de définir le comportement et les effets secondaires des instructions afin que le moteur de décompilation de SLEIGH puissent générer du pseudocode C à partir d'un programme en PEP/8. Ceci requiert la définition de conventions d'appel afin que l'on puisse identifier le passage de paramètres et les valeurs de retour de fonctions.

Le comportement des instructions est défini avec le langage SLEIGH. Celui-ci ressemble à un _Register Transfer Language_. On représente donc ce qui se produit à l'aide d'opérations de base et du transfert de données entre des registres et emplacements en mémoire. Tous les effets des instructions doivent définis explicitement; autant le comportement principal que les effets secondaires comme les changements du registre d'état et le déplacement du pointeur de pile.

Notre définition est présentement incomplète. Ceci est principalement dû à un manque de temps en raison de l'ampleur du travail requis par le projet.

En effet, les _flags_ d'état _Overflow (V)_ et _Carry (C)_ ne sont présentement pas gérés. Nous ne connaissons pas tous les détails des algorithmes utilisés, mais selon nous, ceci devrait avoir peu d'impact sur les résultats de la décompilation, mais pourrait changer la représentation du flux de contrôle du programme quand les instructions `BRC` ou `BRV` sont utilisées.

Plus important, les instructions qui utilisent le mécanisme de _trap_ du processeur (similaire au mécanisme de _syscalls_) ne sont pas implémentées. Ce type d'instruction est plus complexe à définir et son implémentation n'est pas abordée dans la documentation officielle. Les instructions concernées sont celles qui touchent l'entrée/sortie et l'instruction `STOP` qui agit comme le _syscall_ `exit` de Linux.

La gestion des modes d'adressage a été complexe pour cette partie du projet. En effet, notre première implémentation à l'aide _macros_ SLEIGH ajoutait un grand nombre de blocs de contrôle inutiles au résultat de la décompilation pour chaque instruction utilisant les modes d'adressage. À cause de ces blocs, le pseudocode généré était presque totalement inutile pour l'analyse en raison de sa complexité et de sa taille.


# Conclusion

Nous avions sous-estimé la quantité de travail requise pour réaliser ce projet, mais considérons qu'il présente tout de même une valeur intéressante dans son état actuel. Bien qu'il s'agisse d'une architecture qui est utilisée presque uniquement dans un contexte d'apprentissage, nous voyons certaines applications pratiques de sa décompilation. En effet, nous croyons que la génération de pseudocode C à partir d'un programme PEP/8 pourrait accélerer la correction de travaux en permettant au correcteur.trice de pouvoir évaluer rapidement la structure du programme et des algorithmes utilisés. La décompilation pourrait aussi aider les étudiant.e.s, qui sont généralement plus familier avec les langagages de haut niveau, à mieux comprendre l'association entre les constructions de haut niveau déjà apprises et le langage d'assemblage qui leur est enseigné.

En dehors du contexte d'enseignement, il serait bénéfique à la communauté d'utilisateurs de SLEIGH de généraliser notre cadriciel de tests unitaires. Nous croyons qu'il serait réalisable d'en faire une version pouvant être appliquée à n'importe quelle architecture. Cette éventuelle version pourrait s'appliquer à une nouvelle architecture sans modification au code lui-même, mais seulement avec des changements de configuration. Plusieurs des informations nécessaires pourrait être récupérées directement des fichiers SLEIGH de l'architecture visée. Avec ce cadre de test, il serait possible d'appliquer certains principes de génie logiciel, tel que les tests unitaires et l'intégration continue, à la définition d'architecture de processeurs pour la rétroingénierie.


# Bibliographie

J. Stanley Warford. _Fourth Ed Pep/8 Documentation_. (Février 2010). \url{http://computersystemsbook.com/wp-content/uploads/2016/03/ReferenceHandout.pdf}

J. Stanley Warford and Ryan Okelberry. _Pep8CPU: a programmable simulator for a central processing unit_. SIGCSE Bull. 39, 1 (Mars 2007), 288–292. DOI:\url{https://doi.org/10.1145/1227504.1227413}

John Toterhi. _Emulating Ghidra’s PCode: Why/How_. \url{https://medium.com/@cetfor/emulating-ghidras-pcode-why-how-dd736d22dfb}

NSA. _Ghidra Scripting_. (28 Février 2019) \url{https://ghidra.re/courses/GhidraClass/Intermediate/Scripting.html}

NSA. _Ghidra Software Reverse Engineering Framework_. Github. \url{https://github.com/NationalSecurityAgency/ghidra}

NSA. _SLEIGH - A Language for Rapid Processor Specification_. (1er septembre 2017). \url{https://ghidra.re/courses/languages/html/sleigh.html}

# Annexe

Tout le code du projet est disponible sur le dépôt Gitlab du projet: [https://gitlab.com/CycleOfTheAbsurd/pep8-decompiler-sleigh](https://gitlab.com/CycleOfTheAbsurd/pep8-decompiler-sleigh) et est régi par la licence Apache 2.0
