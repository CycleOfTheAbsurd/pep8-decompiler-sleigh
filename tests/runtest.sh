#!/bin/bash

f=$1

output=`${GHIDRA_INSTALL_DIR}support/analyzeHeadless . test -import $f -scriptPath ../Unit-Testing/ghidra_scripts -postScript emulate.py -processor 'Pep/8:BE:16:default' -deleteProject`
ghidraRetVal=$?
echo -e "$output"
grep "FAIL-octopus-computing-arrange" 1>/dev/null <<< "$output";
exit $((ghidraRetVal | ($? == 0) )) # Fail if either the script or Ghidra has failed
