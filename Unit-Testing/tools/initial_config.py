#!/usr/bin/env python2
# -*- encoding: utf-8 -*-
import site
from distutils.spawn import find_executable
from os import environ
from os.path import abspath,dirname,realpath

lib_folders = site.getsitepackages()
ghidra_folder = dirname(abspath(realpath(find_executable("ghidra")))) + "/"

environ["GHIDRA_INSTALL_DIR"] = ghidra_folder
lines = ["GHIDRA_INSTALL_DIR ?= {}".format(ghidra_folder),
         "export GHIDRA_INSTALL_DIR"]
with open("Makefile", "a") as makefile:
    makefile.write("\n".join(lines) + "\n")

with open("lib_paths", "w") as config_file:
    config_file.write("\n".join(lib_folders) + "\n")
