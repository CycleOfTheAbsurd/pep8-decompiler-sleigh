# -*- encoding: utf-8 -*-
from __future__ import print_function
from collections import OrderedDict
from itertools import count
import struct
import sys
import os.path
from os import getcwd
with open(os.path.join(os.getcwd(), ".." , "lib_paths")) as lib_path_file:
    for line in lib_path_file.readlines():
        sys.path.append(line.rstrip())
import json5 as json

from ghidra.app.emulator import EmulatorHelper
from ghidra.util.task import ConsoleTaskMonitor
from ghidra.util.task import ConsoleTaskMonitor

def FAIL(msg):
    print(msg, file=sys.stderr)
    print("FAIL-octopus-computing-arrange") # Ghidra headless mode doesn't return the scripts exit value, so we need a sentinel value to check if there was an error during execution

def initialize_reg(emuHelper):
    emuHelper.writeRegister(emuHelper.getPCRegister(), 0x0)
    emuHelper.writeRegister("rA", 0x0)
    emuHelper.writeRegister("rX", 0x0)
    emuHelper.writeRegister("sp", 0xFBCF) # Inital value set in pep/8 emulator
    emuHelper.writeRegister("NF", 0x0)
    emuHelper.writeRegister("ZF", 0x0)
    emuHelper.writeRegister("VF", 0x0)
    emuHelper.writeRegister("CF", 0x0)
    return emuHelper

def get_address(offset):
    return currentProgram.getAddressFactory().getDefaultAddressSpace().getAddress(offset)

def program_state_to_dict(emuHelper):
    state = {"registers": OrderedDict(), "stack":[], "emuH":emuHelper}
    for reg in ["rA", "rX", "NF", "ZF", "VF", "CF"]: #Signed registers
        state["registers"][reg] = read_register_signed(emuHelper, reg)
    for reg in ["pc", "sp"]: #Unsigned registers
        state["registers"][reg] = emuHelper.readRegister(reg)
    for offset in range(0,16,2):
        bytes_val = emuHelper.readMemory(get_address(state["registers"]["sp"] + offset), 2)
        actual_val = struct.unpack(">h", bytes(bytearray(bytes_val)))[0]
        state["stack"].append(actual_val)
    return state

def read_register_signed(emuHelper, reg):
    n = emuHelper.readRegister(reg)
    if n > 0x7FFF:
        n = - 0x10000 + (n & 0xFFFF)
    return n


"""
Prints the value of all registers and the top of the stack
"""
def show_processor_state(stateDict):
    fmt = """
-REGISTERS------------------------------------------
A : {:#04x}
X : {:#04x}
pc: {:#04x}
sp: {:#04x}
FLAGS: Neg: {} | Zero: {} | Overflow: {} | Carry: {}
-STACK----------------------------------------------
"""
    stackfmt = "{:#04x}|+{:#02x}: {}"
    print(fmt.format(*stateDict["registers"].values()), end="")
    for i in range(len(stateDict["stack"])):
        print(stackfmt.format(stateDict["registers"]["sp"]+i*2, i*2, stateDict["stack"][i]))

def emulate_step():
    monitor = ConsoleTaskMonitor()
    emuHelper = EmulatorHelper(currentProgram)
    emuHelper = initialize_reg(emuHelper)

    while not monitor.isCancelled():
        executionAddress = emuHelper.getExecutionAddress()

        success = emuHelper.step(monitor)

        if not success:
            lastError = emuHelper.getLastError()
            FAIL("Emulation Error: '{}'".format(lastError))
            sys.exit(1)
        yield program_state_to_dict(emuHelper)
    emuHelper.dispose()

# If a field's expected value is None (null in json) do not validate it. A null value cannot actually happen is Pep/8, so it is used to indicate that this field's value doesn't matter for that test.
def validate_step(stateDict, expected):
    err = 0
    if stateDict["registers"] is not None:
        for k in stateDict["registers"].keys():
            if expected["registers"][k] is not None:
                err += stateDict["registers"][k] != expected["registers"][k]
    if expected["stack"] is not None:
        for act,exp in zip(stateDict["stack"], expected["stack"]):
            err += act != exp
    if "addr" in expected:
        for addr,(val,size) in expected["addr"].items():
            # Transform array of byte values to a single int
            actual_val = bytes(bytearray(stateDict["emuH"].readMemory(get_address(int(addr,0)), size)))
            fmt = "b" if size == 1 else "h" # Works only for 16bit processors
            actual_val = struct.unpack(">" + fmt, actual_val)[0]
            err += val != actual_val
    return err

if __name__ == "__main__":
    display = len(sys.argv) >= 2 and "-d" in sys.argv

    resultsFile = os.path.splitext(currentProgram.getExecutablePath())[0] + ".exp"
    with open(resultsFile) as f:
        expected = json.load(f)["states"]
    for i,s,exp in zip(count(), emulate_step(), expected):
        if display:
            show_processor_state(s)
        if validate_step(s,exp) != 0:
            FAIL("Error at step {}: \nExpected {}\nBut got {}\n".format(i, exp, s))
