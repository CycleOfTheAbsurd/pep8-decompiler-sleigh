# PEP/8 Decompiler

This is a decompiler for the [PEP/8](http://computersystemsbook.com/4th-edition/pep8/) educational assembly language. It is developped as the final project for the [INF889A](https://info.uqam.ca/~privat/INF889A/) class.

## Building

TODO

## Using the decompiler

TODO

## Ghidra Integration

TODO

## Dependencies

 - [Ghidra](https://ghidra-sre.org/) for pretty much everything.

### Build dependencies

 - [Pep8Term](https://github.com/CycleOfTheAbsurd/pep8term) to compile unit tests.

## Contributing

### Writing tests

To add a new test you must create 2 files in the `tests/` directory:

1. A `.pep` file containing the Pep/8 code to run.
2. A `.exp` file containing the expected state of registers and the stack after each instruction. This is a JSON file containing a `states` array with one entry per instruction. You can use [tests/test-ld_i.exp](tests/test-ld_i.exp) as a template. A value of `null` in any field is used to indicated that this field doesn't need to be validated at that point.

## References

 -  J\. Stanley Warford. 2010. _Fourth Ed Pep/8 Reference Handout_. [http://computersystemsbook.com/wp-content/uploads/2016/03/ReferenceHandout.pdf](http://computersystemsbook.com/wp-content/uploads/2016/03/ReferenceHandout.pdf)
 -  J\. Stanley Warford and Ryan Okelberry. 2007. _Pep8CPU: a programmable simulator for a central processing unit_. SIGCSE Bull. 39, 1 (March 2007), 288–292. DOI:https://doi.org/10.1145/1227504.1227413. [http://www.cslab.pepperdine.edu/warford/CSUploads/Pep8CPUPaper.pdf](http://www.cslab.pepperdine.edu/warford/CSUploads/Pep8CPUPaper.pdf)
 - NSA. 2017. _SLEIGH - A Language for Rapid Processor Specification_. [https://ghidra.re/courses/languages/html/sleigh.html](https://ghidra.re/courses/languages/html/sleigh.html)
 - NSA. _GhidraDocs_. [https://ghidra.re/ghidra_docs/api/index.html](https://ghidra.re/ghidra_docs/api/index.html)
 - John Toterhi. _Emulating Ghidra’s PCode: Why/How_. [https://medium.com/@cetfor/emulating-ghidras-pcode-why-how-dd736d22dfb](https://medium.com/@cetfor/emulating-ghidras-pcode-why-how-dd736d22dfb)
 - NSA. _Headless Analyzer README_. [https://ghidra.re/ghidra_docs/analyzeHeadlessREADME.html](https://ghidra.re/ghidra_docs/analyzeHeadlessREADME.html)
 - binpang. _Ghidra Python Uses Other Packages_. [http://blog.binpang.me/2019/08/22/ghidra-python/](http://blog.binpang.me/2019/08/22/ghidra-python/)
 - JSON5 [https://json5.org/](https://json5.org/)
 	> Numbers may be hexadecimal.
